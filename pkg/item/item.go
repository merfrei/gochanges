// Package item represents an item to process
package item

import "go.mongodb.org/mongo-driver/mongo"

// Item interface
type Item interface {
	Compare(*Item, map[string]interface{})
	Add(*mongo.Database) error
	Update(*mongo.Database, map[string]interface{}) error
	Check(*mongo.Database) error
	Archive(*mongo.Database) error
}

// Collections is the config for collection names in MongoDB
type Collections struct {
	Data    string
	History string
}
