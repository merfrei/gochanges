package item

import (
	"context"
	"strings"
	"time"

	"github.com/fatih/structs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// ProductCollections are the collections used for storing products data
var ProductCollections = Collections{
	Data:    "products.data",
	History: "products.history",
}

// Variable fields will be checked for changes
var varFields = []string{"Name", "URL", "Price", "SKU", "EAN", "Category", "Brand", "ImageURL", "Shipping", "Stock", "Dealer"}

// Product represents a product item in the database
type Product struct {
	Identifier string    `json:"identifier" csv:"identifier"`
	Viewed     time.Time `bson:"viewed" json:"viewed,omitempty" csv:"viewed,omitempty"`
	Name       string    `json:"name,omitempty" csv:"name,omitempty"`
	URL        string    `json:"url,omitempty" csv:"url,omitempty"`
	Price      string    `json:"price,omitempty" csv:"price,omitempty"`
	SKU        string    `json:"sku,omitempty" csv:"sku,omitempty"`
	EAN        string    `json:"ean,omitempty" csv:"ean,omitempty"`
	Category   string    `json:"category,omitempty" csv:"category,omitempty"`
	Brand      string    `json:"brand,omitempty" json:"brand,omitempty"`
	ImageURL   string    `json:"imageURL,omitempty" csv:"imageURL,omitempty"`
	Shipping   string    `json:"shipping,omitempty" csv:"shipping,omitempty"`
	Stock      string    `json:"stock,omitempty" csv:"stock,omitempty"`
	Dealer     string    `json:"dealer,omitempty" csv:"dealer,omitempty"`
}

// Compare a product with another one and return the differences
func (p *Product) Compare(product *Product, changes map[string]interface{}) {
	productM1 := structs.Map(p)
	productM2 := structs.Map(product)

	for _, field := range varFields {
		if productM1[field] != productM2[field] {
			changes[strings.ToLower(field)] = productM2[field]
		}
	}
}

// Add a new product
func (p *Product) Add(db *mongo.Database) error {
	_, err := db.Collection(ProductCollections.Data).InsertOne(context.Background(), p)
	return err
}

// Update a product with the changes
func (p *Product) Update(db *mongo.Database, changes map[string]interface{}) error {
	if len(changes) == 0 {
		return nil
	}
	filter := bson.M{"identifier": p.Identifier}
	update := bson.D{}
	for field, value := range changes {
		// we know all the values are a string
		update = append(update, bson.E{Key: field, Value: value.(string)})
	}
	_, err := db.Collection(ProductCollections.Data).UpdateOne(
		context.Background(), filter, bson.D{
			{Key: "$set", Value: update}})
	return err
}

// Check update the Viewed field for a product to time.Now() value
func (p *Product) Check(db *mongo.Database) error {
	_, err := db.Collection(ProductCollections.Data).UpdateOne(
		context.Background(), bson.M{"identifier": p.Identifier}, bson.D{
			{Key: "$set", Value: bson.D{
				{Key: "viewed", Value: time.Now()}}}})
	return err
}

// Archive store the current product to history
// It should be run before any other actions such as update, delete or check
func (p *Product) Archive(db *mongo.Database) error {
	_, err := db.Collection(ProductCollections.History).InsertOne(context.Background(), p)
	return err
}
