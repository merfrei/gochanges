// Package feed read items from an input of data
package feed

// Feed read and fetch new data and put it into a channel
type Feed interface {
	Fetch(c interface{}) error
}
