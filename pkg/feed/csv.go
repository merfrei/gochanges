package feed

import (
	"log"
	"os"

	"github.com/gocarina/gocsv"
)

// CSVFeed is a Feed that fetch data from a CSV file
type CSVFeed struct {
	Filename string
}

// Fetch method for the Feed interface
func (csvF *CSVFeed) Fetch(c interface{}) {
	csvFile, err := os.Open(csvF.Filename)
	if err != nil {
		log.Println("ERROR FOUND: ", err)
		return
	}
	defer csvFile.Close()

	if err := gocsv.UnmarshalToChan(csvFile, c); err != nil {
		log.Println("ERROR FOUND: ", err)
	}
}
