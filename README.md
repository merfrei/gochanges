Changes Processor In Go
=======================

This is a project I made to try some things in Go.

## How should it work?

We have the main data stored in a MongoDB database. This data is indexed using the `identifier` field.

Then we feed this program with new data and it will detect the changes.

For each row/element:

- If the identifier already exists in the collection, then it will process the changes and update the current product.
- If it does not exist in the collection, then it will add this row as a new document.
- Update the time of viewed.
- Archive the old version of the product.

So, each row in the feed file must have an identifier.


## Init DB

```javascript
use gochanges

db.products.data.createIndex({"identifier": 1})

db.products.history.createIndex({"identifier": 1})
```
