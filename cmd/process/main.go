package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"

	"gitlab.com/merfrei/gochanges/pkg/config"
	"gitlab.com/merfrei/gochanges/pkg/feed"
	"gitlab.com/merfrei/gochanges/pkg/item"
	mongoM "gitlab.com/merfrei/gochanges/pkg/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func main() {
	configF := flag.String("config", "config.toml", "Path to the config file")
	itemsFile := flag.String("items", "products.csv", "The items CSV file")

	flag.Parse()

	configD, err := ioutil.ReadFile(*configF)
	if err != nil {
		log.Fatal(err)
	}
	config := config.Load(configD)

	mongoCli, err := mongoM.Connect(config.Mongo.URI)
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(mongoCli)

	db := mongoCli.Database(config.Mongo.Database)
	col := db.Collection(item.ProductCollections.Data)

	productsChan := make(chan *item.Product)
	csvFeed := feed.CSVFeed{Filename: *itemsFile}

	go csvFeed.Fetch(productsChan)

	for productNew := range productsChan {
		log.Printf("Processing product: %+v\n", productNew)
		productOld := &item.Product{}
		err := col.FindOne(context.Background(), bson.D{{Key: "identifier", Value: productNew.Identifier}}).Decode(productOld)
		if err != nil {
			if err != mongo.ErrNoDocuments {
				log.Fatal(err)
			}
		}
		changes := make(map[string]interface{})
		if productOld.Identifier != "" {
			// Archive history
			err = productOld.Archive(db)
			if err != nil {
				log.Fatal(err)
			}
			// Compute changes
			productOld.Compare(productNew, changes)
		} else {
			// Add a new product
			err = productNew.Add(db)
			if err != nil {
				log.Fatal(err)
			}
		}
		productNew.Update(db, changes)
		productNew.Check(db)
	}
}
