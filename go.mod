module gitlab.com/merfrei/gochanges

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/fatih/structs v1.1.0
	github.com/gocarina/gocsv v0.0.0-20201208093247-67c824bc04d4
	go.mongodb.org/mongo-driver v1.4.5
)
