## Contains DB data and files

Before running MongoDB with the default [mongo.conf](../mongo.conf) file do not forget to create the `mongodb` directory here.
